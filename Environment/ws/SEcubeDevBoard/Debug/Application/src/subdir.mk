################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/adc.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/crc.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/dma.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/fmc.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/gpio.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/i2c.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/main.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/rng.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/sdio.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/se3_rand.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/se3_sdio.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/spi.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/stm32f4xx_hal_msp.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/stm32f4xx_it.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/tim.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/usart.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/usb_device.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/usbd_conf.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/usbd_desc.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/usbd_storage_if.c 

OBJS += \
./Application/src/adc.o \
./Application/src/crc.o \
./Application/src/dma.o \
./Application/src/fmc.o \
./Application/src/gpio.o \
./Application/src/i2c.o \
./Application/src/main.o \
./Application/src/rng.o \
./Application/src/sdio.o \
./Application/src/se3_rand.o \
./Application/src/se3_sdio.o \
./Application/src/spi.o \
./Application/src/stm32f4xx_hal_msp.o \
./Application/src/stm32f4xx_it.o \
./Application/src/tim.o \
./Application/src/usart.o \
./Application/src/usb_device.o \
./Application/src/usbd_conf.o \
./Application/src/usbd_desc.o \
./Application/src/usbd_storage_if.o 

C_DEPS += \
./Application/src/adc.d \
./Application/src/crc.d \
./Application/src/dma.d \
./Application/src/fmc.d \
./Application/src/gpio.d \
./Application/src/i2c.d \
./Application/src/main.d \
./Application/src/rng.d \
./Application/src/sdio.d \
./Application/src/se3_rand.d \
./Application/src/se3_sdio.d \
./Application/src/spi.d \
./Application/src/stm32f4xx_hal_msp.d \
./Application/src/stm32f4xx_it.d \
./Application/src/tim.d \
./Application/src/usart.d \
./Application/src/usb_device.d \
./Application/src/usbd_conf.d \
./Application/src/usbd_desc.d \
./Application/src/usbd_storage_if.d 


# Each subdirectory must supply rules for building sources it contributes
Application/src/adc.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/adc.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/crc.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/crc.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/dma.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/dma.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/fmc.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/fmc.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/gpio.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/gpio.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/i2c.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/i2c.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/main.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/main.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/rng.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/rng.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/sdio.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/sdio.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/se3_rand.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/se3_rand.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/se3_sdio.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/se3_sdio.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/spi.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/spi.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/stm32f4xx_hal_msp.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/stm32f4xx_hal_msp.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/stm32f4xx_it.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/stm32f4xx_it.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/tim.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/tim.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/usart.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/usart.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/usb_device.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/usb_device.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/usbd_conf.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/usbd_conf.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/usbd_desc.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/usbd_desc.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/usbd_storage_if.o: /Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/usbd_storage_if.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SEcube_SDK/SEcubeSDK_GAF_14gen2017/SEcube_SDK/Development/Environment/secube/src/Device"  -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


