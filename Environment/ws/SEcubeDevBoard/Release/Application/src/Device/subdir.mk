################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_algo_Aes.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_algo_AesHmacSha256s.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_algo_HmacSha256.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_algo_aes256hmacsha256.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_algo_sha256.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_cmd.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_cmd0.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_cmd1.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_cmd1_config.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_cmd1_crypto.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_cmd1_keys.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_cmd1_login.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_flash.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_keys.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_memory.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_proto.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3c0.c \
/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3c1.c 

OBJS += \
./Application/src/Device/se3_algo_Aes.o \
./Application/src/Device/se3_algo_AesHmacSha256s.o \
./Application/src/Device/se3_algo_HmacSha256.o \
./Application/src/Device/se3_algo_aes256hmacsha256.o \
./Application/src/Device/se3_algo_sha256.o \
./Application/src/Device/se3_cmd.o \
./Application/src/Device/se3_cmd0.o \
./Application/src/Device/se3_cmd1.o \
./Application/src/Device/se3_cmd1_config.o \
./Application/src/Device/se3_cmd1_crypto.o \
./Application/src/Device/se3_cmd1_keys.o \
./Application/src/Device/se3_cmd1_login.o \
./Application/src/Device/se3_flash.o \
./Application/src/Device/se3_keys.o \
./Application/src/Device/se3_memory.o \
./Application/src/Device/se3_proto.o \
./Application/src/Device/se3c0.o \
./Application/src/Device/se3c1.o 

C_DEPS += \
./Application/src/Device/se3_algo_Aes.d \
./Application/src/Device/se3_algo_AesHmacSha256s.d \
./Application/src/Device/se3_algo_HmacSha256.d \
./Application/src/Device/se3_algo_aes256hmacsha256.d \
./Application/src/Device/se3_algo_sha256.d \
./Application/src/Device/se3_cmd.d \
./Application/src/Device/se3_cmd0.d \
./Application/src/Device/se3_cmd1.d \
./Application/src/Device/se3_cmd1_config.d \
./Application/src/Device/se3_cmd1_crypto.d \
./Application/src/Device/se3_cmd1_keys.d \
./Application/src/Device/se3_cmd1_login.d \
./Application/src/Device/se3_flash.d \
./Application/src/Device/se3_keys.d \
./Application/src/Device/se3_memory.d \
./Application/src/Device/se3_proto.d \
./Application/src/Device/se3c0.d \
./Application/src/Device/se3c1.d 


# Each subdirectory must supply rules for building sources it contributes
Application/src/Device/se3_algo_Aes.o: /Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_algo_Aes.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device"  -O3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/Device/se3_algo_AesHmacSha256s.o: /Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_algo_AesHmacSha256s.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device"  -O3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/Device/se3_algo_HmacSha256.o: /Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_algo_HmacSha256.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device"  -O3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/Device/se3_algo_aes256hmacsha256.o: /Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_algo_aes256hmacsha256.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device"  -O3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/Device/se3_algo_sha256.o: /Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_algo_sha256.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device"  -O3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/Device/se3_cmd.o: /Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_cmd.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device"  -O3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/Device/se3_cmd0.o: /Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_cmd0.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device"  -O3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/Device/se3_cmd1.o: /Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_cmd1.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device"  -O3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/Device/se3_cmd1_config.o: /Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_cmd1_config.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device"  -O3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/Device/se3_cmd1_crypto.o: /Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_cmd1_crypto.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device"  -O3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/Device/se3_cmd1_keys.o: /Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_cmd1_keys.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device"  -O3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/Device/se3_cmd1_login.o: /Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_cmd1_login.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device"  -O3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/Device/se3_flash.o: /Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_flash.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device"  -O3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/Device/se3_keys.o: /Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_keys.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device"  -O3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/Device/se3_memory.o: /Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_memory.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device"  -O3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/Device/se3_proto.o: /Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3_proto.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device"  -O3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/Device/se3c0.o: /Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3c0.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device"  -O3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/src/Device/se3c1.o: /Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device/se3c1.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=c99 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F429xx -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Common" -I"/Dati/GoogleDrive/Uni/5-2/SDP/SDP/Environment/secube/src/Device"  -O3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


