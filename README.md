# Introduction

**This procedure has only been tested on GNU/Linux.**

## Download the source

Fetch this repository using

```bash
    git clone --recursive https://gitlab.com/marco.magliona/SDP.git
```

## Compiling guide

### Native compilation (the produced executable can be used directly)
Navigate to the *telegram* folder with

```bash
    cd telegram
```

Please create the **Libraries** folder inside the **telegram** folder.
Follow the normal Telegram compilation instruction [here](https://gitlab.com/marco.magliona/tdesktop/blob/selegram/docs/building-cmake.md).

The building system of Telegram has been modified to include, compile and link the SEfile libraries.
The process is fully automated and exactly the same as the one for standard Telegram.

### Docker compilation (the produced executable can only be used inside Docker)
We have a preconfigured Docker image with the development environment configured and all the libraries installed. You can download it [here](https://mega.nz/#!89sjXBDT!9Cb_BUemhdSl7aAnXMhFs-6rTvIIeV7SEA2diPAK_d8) (DO NOT UNPACK THE tar FILE).
You can download the precompiled libraries to be used with the docker image [here](https://mega.nz/#!x4lVXLJI!Izk-2fxmiSrcM0ILa-6cOFGyyL2HhRE_1ChoVT2uVls) and unpack them inside *telegram* folder.

Import the docker image with  
```bash
    docker load -i telegramdev.tar
```

You can run it with

```bash
    docker run -t -i --rm --privileged -v /<Path to the downloaded SDP repository>/SDP:/SDP \  
    -v /<Path to where the removable devices are mounted>:/run/media -e DISPLAY=$DISPLAY \  
    -v /tmp/.X11-unix:/tmp/.X11-unix telegram_dev:latest /bin/bash
```

Inside the root of the container is present a script which launches the compilation and produces the final SElegram executable.

```bash
   ./compileTelegram.sh
```

## Running SElegram
If you have compiled using the Docker image provided.

1. Connect the board before launching the Docker image
2. Run the command 
```bash
   xhost +local:docker
```

3. Run the Docker image as shown above and when you get the shell prompt launch SElegram with 
```bash
    Telegram
```
If nothing is shown, close the Docker container and run the command 
```bash
xhost +
```
then go to 3.

4. A login prompt to enable SEfile is shown. Insert the password associated with the SEcube board.  
5. For the encryption/decryption to work set the Download folder in Telegram to **/tmp/** from the Settings menu.

If you are not using the provided Docker image you can find the final executable inside SDP/telegram/out/Debug. You can run it with

```bash
    cd <Path to the SDP folder>/telegram/out/Debug
    ./Telegram
```

Now you can go to step 4 and 5.

### Reading incoming messages
To decrypt the incoming messages you need to manually download the received file (by design).

### Sending messages
No differences with the normal Telegram application. Please notice that two "ballons" per each sent message will be generated.

## Telegram autogenenerated documentation
The documentation, generated in Doxygen, we have used to analize the Telegram source code can be downloaded from [here](https://mega.nz/#!Rg0jlR4B!_d_Re616HBikhKXy52JZ_v9k4BHHztHH7KaLamtUF4A). Uncompress the archive and open *index.html* .
