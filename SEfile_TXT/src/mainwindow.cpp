#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    LoginDialog* loginDialog = new LoginDialog( this );
    loginDialog->exec();

    if(loginDialog->result() == QDialog::Rejected){

        exit(1);
    }
    se3_session* tmp=loginDialog->getSession();
    memcpy(&s, tmp, sizeof(se3_session));
    if(L1_crypto_set_time(&s, (uint32_t)time(0))){

        exit(1);
    }
    if(secure_init(&s, -1, SE3_ALGO_MAX+1)){

        exit(1);
    }
    ui->setupUi(this);
    //connect (ui->actionSetEnvironment, SIGNAL(triggered(bool))), this, SLOT(dialogSetEnvironment()));
}

MainWindow::~MainWindow()
{
    secure_finit();
    if(s.logged_in)
        L1_logout(&s);
    delete ui;
}
bool MainWindow::manageOpenSaveFile(bool newfile){
    if(newfile)
        path = QFileDialog::getSaveFileName(this, "Save Plain Text File", ".", "Text file (*.txt)");
    else
        path = QFileDialog::getOpenFileName(this, "Open Plain Text File", ".", "Text file (*.txt)");

    if(path.isNull()) return false;

    filename = QFileInfo(QFile(path)).fileName();

    if(filename.isNull()) return false;
    path = path.left(path.size() - filename.size());
    return true;
}

void MainWindow::on_openPlain_btn_clicked()
{
    char *enc_filename = NULL;
    uint8_t *buffer = NULL;
    uint16_t len_enc_filename = 0, ret = 0;
    QString *path_encfile;
    QString *path_plainfile;
    int size_len = 0;
    QString file_content;


    on_close_btn_clicked();
    if(!manageOpenSaveFile(false)) return;
    enc_filename = (char *)calloc((B5_SHA256_DIGEST_SIZE*2+1),sizeof(char));
    if(enc_filename == NULL) return;


    path_plainfile = new QString(path);
    path_plainfile->append(filename);
    plain_file = new QFile(*path_plainfile);
    plain_file->open(QIODevice::ReadOnly);
    file_content = plain_file->readAll();
    if((ret = secure_open(path_plainfile->toUtf8().data(), &sefile_file, SEFILE_WRITE, SEFILE_NEWFILE))){
        if ( ret == SEFILE_SIGNATURE_MISMATCH ){
            ui->plainName_lbl->setText("Signature Mismatch");
            ui->encName_lbl->setText("Signature Mismatch");
        }else{
            ui->plainName_lbl->setText("Error code: " + ret);
            ui->encName_lbl->setText("Error code: " + ret);
        }
        return; // todo error
    }


    crypto_filename(filename.toUtf8().data(), enc_filename, &len_enc_filename);
    path_encfile = new QString(path);
    path_encfile->append(enc_filename);
    cipher_file = new QFile(*path_encfile);
    cipher_file->open(QIODevice::ReadWrite);

    ui->plainName_lbl->setText(filename);
    ui->plain_txte->setText(file_content);
    size_len = file_content.size();
    buffer = (uint8_t *) malloc(size_len*sizeof(uint8_t));
    if (buffer == NULL ) {
       return; // todo error
    }

    memcpy(buffer, file_content.toUtf8().data(), size_len);
    if((ret = secure_write(&sefile_file, buffer, size_len))) {
        if ( ret == SEFILE_SIGNATURE_MISMATCH ){
            ui->plainName_lbl->setText("Signature Mismatch");
            ui->encName_lbl->setText("Signature Mismatch");
        }else{
            ui->plainName_lbl->setText("Error code: " + ret);
            ui->encName_lbl->setText("Error code: " + ret);
        }
        free(buffer);
        free(enc_filename);
        return; // todo error
    }
    ui->encName_lbl->setText(QString(enc_filename));
    QByteArray cipher_content = cipher_file->readAll().toHex();
    ui->enc_txte->setText(QString(cipher_content));
    secure_close(&sefile_file);
    plain_file->close();
    cipher_file->close();
    free(buffer);
    free(enc_filename);

}


void MainWindow::on_save_btn_clicked()
{
    char *buffer = NULL;
    char *enc_filename = NULL;
    uint16_t len_enc_filename = 0 ;
    QString *path_encfile;
    QString *path_plainfile;
    uint16_t ret = SE3_OK;
    bool do_resize = false;
    if(filename.size() == 0){
        if(!manageOpenSaveFile(true)) return;
    }

    enc_filename = (char *)calloc((B5_SHA256_DIGEST_SIZE*2+1),sizeof(char));
    if(enc_filename == NULL) return;

    crypto_filename(filename.toUtf8().data(), enc_filename, &len_enc_filename);

    path_plainfile = new QString(path);
    path_plainfile->append(filename);
    if(QFile::exists(*path_plainfile)) do_resize = true;
    plain_file = new QFile(*path_plainfile);
    plain_file->open(QIODevice::ReadWrite);
    if(!plain_file->isOpen()) {
        free(enc_filename);
        return;
    }
    if(do_resize) plain_file->resize(0);

    if((ret = secure_open(path_plainfile->toUtf8().data(), &sefile_file, SEFILE_WRITE, SEFILE_NEWFILE))){
        if ( ret == SEFILE_SIGNATURE_MISMATCH ){
            ui->plainName_lbl->setText("Signature Mismatch");
            ui->encName_lbl->setText("Signature Mismatch");
        }else{
            ui->plainName_lbl->setText("Error code: " + ret);
            ui->encName_lbl->setText("Error code: " + ret);
        }
        free(enc_filename);
        return; // todo error
    }
    path_encfile = new QString(path);
    path_encfile->append(enc_filename);
    cipher_file = new QFile(*path_encfile);
    cipher_file->open(QIODevice::ReadWrite);
    if(!cipher_file->isOpen()) {
        free(enc_filename);
        return;
    }
    //plain_file->seek(0);
    QString tmp = ui->plain_txte->toPlainText();
//    plain_file->write(tmp);
    QTextStream plain_out(plain_file);
    plain_out << tmp;

    buffer = (char *) malloc(ui->plain_txte->toPlainText().size()*sizeof(char));
    if (buffer == NULL ) {
        free(enc_filename);
        return;
    }
    memcpy(buffer, ui->plain_txte->toPlainText().toUtf8().data(), ui->plain_txte->toPlainText().size());
    if((ret = secure_write(&sefile_file, (uint8_t*)buffer, ui->plain_txte->toPlainText().size()))) {
        if ( ret == SEFILE_SIGNATURE_MISMATCH ){
            ui->plainName_lbl->setText("Signature Mismatch");
            ui->encName_lbl->setText("Signature Mismatch");
        }else{
            ui->plainName_lbl->setText("Error code: " + ret);
            ui->encName_lbl->setText("Error code: " + ret);
        }
        free(buffer);
        free(enc_filename);
        return; // todo error
    }

    ui->enc_txte->setText(QString(cipher_file->readAll().toHex()));
    free(buffer);
    free(enc_filename);
    secure_close(&sefile_file);
    plain_file->close();
    cipher_file->close();

}

void MainWindow::on_openCipher_btn_clicked()
{
    QString fileContent;
    uint8_t buffer[SEFILE_LOGIC_DATA];
    char *enc_filename = NULL;
    uint32_t nBytesRead = 0;
    uint16_t len_enc_filename = 0, ret = 0;
    QString *path_encfile;
    QString *path_plainfile;
    bool do_resize = false;

    on_close_btn_clicked();
    SecureFileDialog *fileDialog = new SecureFileDialog( this, 0 );
    fileDialog->exec();
    if(fileDialog->result()==QDialog::Rejected){
        return ;
    }
    path = fileDialog->getChosenFile();
    filename = QFileInfo(QFile(path)).fileName();

    if(filename.isNull()) return;
    path = path.left(path.size() - filename.size());

    path_plainfile = new QString(path);
    path_plainfile->append(filename);
    if(QFile::exists(*path_plainfile)){
        do_resize = true;
    }
    plain_file = new QFile(*path_plainfile);
    plain_file->open(QIODevice::ReadWrite);
    if(do_resize)
        plain_file->resize(0);

    if((ret = secure_open(path_plainfile->toUtf8().data(), &sefile_file, SEFILE_WRITE, SEFILE_OPEN))){
        if ( ret == SEFILE_SIGNATURE_MISMATCH ){
            ui->plainName_lbl->setText("Signature Mismatch");
            ui->encName_lbl->setText("Signature Mismatch");
        }else{
            ui->plainName_lbl->setText("Error code: " + ret);
            ui->encName_lbl->setText("Error code: " + ret);
        }
        return; // todo error
    }

    enc_filename = (char *)calloc((B5_SHA256_DIGEST_SIZE*2+1),sizeof(char));
    if(enc_filename == NULL) return;

    crypto_filename(filename.toUtf8().data(), enc_filename, &len_enc_filename);

    path_encfile = new QString(path);
    path_encfile->append(enc_filename);
    cipher_file = new QFile(*path_encfile);
    cipher_file->open(QIODevice::ReadOnly);
    //memset(buffer, '0', SEFILE_LOGIC_DATA);
    while((ret = secure_read(&sefile_file,  buffer, SEFILE_LOGIC_DATA, &nBytesRead)) == 0 && nBytesRead>0){
        QString tmp(QString::fromUtf8((char *)buffer, nBytesRead));
        fileContent.append(tmp);
    }
    if ( ret == SEFILE_SIGNATURE_MISMATCH ){
        ui->plainName_lbl->setText("Signature Mismatch");
        ui->encName_lbl->setText("Signature Mismatch");
        free(enc_filename);
        secure_close(&sefile_file);
        plain_file->close();
        cipher_file->close();
        return;
    }else if (ret > 0){
        ui->plainName_lbl->setText("Error code: " + ret);
        ui->encName_lbl->setText("Error code: " + ret);
        free(enc_filename);
        secure_close(&sefile_file);
        plain_file->close();
        cipher_file->close();
        return;
    }
//    plain_file->write(fileContent.toUtf8());
    ui->plainName_lbl->setText(filename);
    ui->plain_txte->setText(fileContent);
    ui->encName_lbl->setText(QString(enc_filename));
    ui->enc_txte->setText(cipher_file->readAll().toHex());
    QTextStream plain_out(plain_file);
    plain_out << fileContent;
    free(enc_filename);
    secure_close(&sefile_file);
    plain_file->close();
    cipher_file->close();

}

void MainWindow::on_actionSetEnvironment_triggered()
{
    EnvironmentDialog *envDialog = new EnvironmentDialog(this, &s);
    envDialog->exec();
}

void MainWindow::on_plain_txte_textChanged()
{
    if (ui->plain_txte->toPlainText().size() > 0){
        ui->save_btn->setEnabled(true);
    }
}

void MainWindow::on_close_btn_clicked()
{
    ui->plainName_lbl->setText("[PlainText_FileName]");
    ui->encName_lbl->setText("[CipherText_FileName]");
    ui->plain_txte->setText("");
    ui->enc_txte->setText("");
    filename.clear();

    ui->save_btn->setEnabled(false);
}
