#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QFileDialog>
#include <QTextDocument>
#include <QTextStream>
#include "SEfile.h"
#include "logindialog.h"
#include "environmentdialog.h"
#include "securefiledialog.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private slots:
    void on_openPlain_btn_clicked();
    void on_save_btn_clicked();
    void on_openCipher_btn_clicked();

    void on_actionSetEnvironment_triggered();

    void on_plain_txte_textChanged();

    void on_close_btn_clicked();

private:
    Ui::MainWindow *ui;
    QString path, filename;
    QFile *plain_file, *cipher_file;
    SEFILE_FHANDLE sefile_file;
    se3_session s;
    se3_key *keyTable;
    se3_algo *algTable;

    bool manageOpenSaveFile(bool newfile);
};

#endif // MAINWINDOW_H
