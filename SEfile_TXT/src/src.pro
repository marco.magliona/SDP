#-------------------------------------------------
#
# Project created by QtCreator 2016-11-08T16:58:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SEfile_demoTXT
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
        environmentdialog.cpp \
        logindialog.cpp \
        securefiledialog.cpp

HEADERS  += mainwindow.h \
            environmentdialog.h \
            logindialog.h \
            securefiledialog.h


FORMS    += mainwindow.ui

win32 {
    SEFILEPATH  = "$$OUT_PWD/../SEfile"
    CONFIG(debug,debug|release){
        SEFILEPATH = $$SEFILEPATH/debug
    }
    CONFIG(release,debug|release){
        SEFILEPATH = $$SEFILEPATH/release
    }
}
!win32 {
    SEFILEPATH  = "../SEfile"
}

LIBS += -L$$SEFILEPATH

LIBS += -lSEfile

INCLUDEPATH += "$$PWD/../SEfile"
DEPENDPATH += "$$PWD/../SEfile" \
            $$SEFILEPATH

CONFIG(release,debug|release){
    CONFIG += static
}
