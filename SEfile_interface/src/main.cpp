#include "sefile_dialog.h"
#include <QApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    SEfileDialog w;
    SEfile_interface *s = w.getSEfile_interface();
    w.show();

    /* Test crypting/encrypting file */
    QString encryptedName;
    QString decryptedName;
    QString keyName;
    /* THE / at the end of the path is important */
    //if(!s->getEncrypted(QString("."), QString("prova.txt"), encryptedName))
    //    QTextStream(stdout) << "Error\n";
    //QTextStream(stdout) << "Encrypted name is " << encryptedName << "\n";
    //QFile file("prova.txt");
    //file.remove();
    //s->getDecrypted(QString("./"),QString("ac9694c9206dd5a9e51e956a07ade297dd9b4a65ff146629aa6cb5aa08eaacd0"), decryptedName);
    //QTextStream(stdout) << "Decrypted name is " << cryptedName << "\n";

    /* List all keys */
    QTextStream(stdout) << "Keys already on board\n";
    se3_key keys[100];
    uint16_t numKeys;
    s->getKeyList(keys, &numKeys);
    for(int i=0; i< numKeys;i++){
        QTextStream(stdout) << "ID : " << keys[i].id << "; Name : " << QString::fromUtf8((char*)keys[i].name, keys[i].name_size) << "\n";
    }

    uint8_t key_test[32] = {
        0,9,1,8, 0,9,1,8, 0,9,1,8, 0,9,1,8,
        0,9,1,8, 0,9,1,8, 0,9,1,8, 0,9,1,8
    };

    QTextStream(stdout) << "Add key test\n";

    s->addKey(QString("test"), key_test, 32, 365*24*3600);

    QTextStream(stdout) << "Keys on board after addition\n";

    s->getKeyList(keys, &numKeys);
    for(int i=0; i< numKeys;i++){
        QTextStream(stdout) << "ID : " << keys[i].id << "; Name : " << QString::fromUtf8((char*)keys[i].name, keys[i].name_size) << "\n";
    }

    QTextStream(stdout) << "Delete the added key\n";
    s->deleteKey(QString("test"));

    QTextStream(stdout) << "Keys on board after deletion\n";

    s->getKeyList(keys, &numKeys);
    for(int i=0; i< numKeys;i++){
        QTextStream(stdout) << "ID : " << keys[i].id << "; Name : " << QString::fromUtf8((char*)keys[i].name, keys[i].name_size) << "\n";
    }

    /* Try to encrypt a file, change key and decrypt */
    keyName = QString("TestKey_1");
    QTextStream(stdout) << "Encrypting with key " << keyName << "\n";
    s->changeKeyID(keyName);
    if(!s->getEncrypted(QString("."), QString("prova.txt"), encryptedName))
        QTextStream(stdout) << "Error\n";
    QTextStream(stdout) << "Encrypted name is " << encryptedName << "\n";
    QFile file("prova.txt");
    file.remove();
    keyName = QString("TestKey_2");
    s->changeKeyID(keyName);
    QTextStream(stdout) << "Switching to key " << keyName << "\n";
    s->getDecrypted(QString("./"), encryptedName, decryptedName);
    if(decryptedName.isEmpty()){
        QTextStream(stdout) << "Cannot decrypt file" << encryptedName << " with key " << keyName << "\n";
    }
    keyName = QString("TestKey_1");
    QTextStream(stdout) << "Switching to key " << keyName << "\n";
    s->changeKeyID(keyName);
    s->getDecrypted(QString("./"), encryptedName, decryptedName);
    if(decryptedName.isEmpty()){
        QTextStream(stdout) << "Cannot decrypt file" << encryptedName << " with key " << keyName << "\n";
    } else {
        QFile file(encryptedName);
        file.remove();
        QTextStream(stdout) << "Decrypt file" << encryptedName << " with key " << keyName << " and name " << decryptedName << "\n";
    }

    return a.exec();
}
