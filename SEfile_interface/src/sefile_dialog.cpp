#include "sefile_dialog.h"
#include "ui_dialog.h"


SEfileDialog::SEfileDialog(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SEfile_dialog)
{
    LoginDialog* loginDialog = new LoginDialog( this );
    loginDialog->setFocusPolicy(Qt::StrongFocus);
    loginDialog->exec();

    if(loginDialog->result() == QDialog::Rejected){
        exit(1);
    }
    s = new SEfile_interface(loginDialog->getSession());
    ui->setupUi(this);
    connect(ui->disconnectButton, SIGNAL(clicked()), this, SLOT(closeWindow()));
}

SEfileDialog::~SEfileDialog()
{
    delete ui;
}

void SEfileDialog::closeWindow(){
    s->logout();
    this->close();
}

SEfile_interface* SEfileDialog::getSEfile_interface(){
    return this->s;
}
