#ifndef WINDOW_H
#define WINDOW_H

#include <QFile>
#include <QTextDocument>
#include <QTextStream>
#include "SEfile.h"
#include "logindialog.h"
#include "sefile_interface.h"
namespace Ui {
class SEfile_dialog;
}

class SEfileDialog : public QWidget
{
    Q_OBJECT

public:
    explicit SEfileDialog(QWidget *parent = 0);
    ~SEfileDialog();
    SEfile_interface* getSEfile_interface();

private slots:
    void closeWindow();

private:
    Ui::SEfile_dialog *ui;
    SEfile_interface *s;
};

#endif // WINDOW_H
