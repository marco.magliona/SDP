#include "sefile_interface.h"

SEfile_interface::SEfile_interface(se3_session* session)
{
    memcpy(&s, session, sizeof(se3_session));
    if(L1_crypto_set_time(&s, (uint32_t)time(0))){
        exit(1);
    }
    if(secure_init(&s, -1, SE3_ALGO_MAX+1)){
        exit(1);
    }
}

void SEfile_interface::logout(){
    secure_finit();
    if(s.logged_in)
        L1_logout(&s);
}

bool SEfile_interface::getEncrypted(QDir path, QString filename, QString &filenameEncrypted)
{
    mutex.lock();
    SEFILE_FHANDLE sefile_file;

    uint16_t len_enc_filename = 0;
    char filenameEncryptedChar[200];
    if(filenameEncryptedChar == NULL) return false;

    // Open original file and copy its content as byte array
    QString plainFilePath = path.filePath(filename);

    QFile *plainFile = new QFile(plainFilePath);
    plainFile->open(QIODevice::ReadOnly);
    QByteArray plainFileContent = plainFile->readAll();
    if(crypto_filename(plainFilePath.toUtf8().data(), filenameEncryptedChar, &len_enc_filename) != 0){
      mutex.unlock();
      return false;
    }

    filenameEncryptedChar[len_enc_filename] = 0;
    filenameEncrypted = QString(filenameEncryptedChar);

    // Open encrypted file
    if(secure_open(plainFilePath.toUtf8().data(), &sefile_file, SEFILE_WRITE, SEFILE_NEWFILE, true) != 0){
      mutex.unlock();
      return false;
    }

    if(secure_write(&sefile_file, (uint8_t*)plainFileContent.data(), plainFileContent.length()) != 0){
        secure_close(&sefile_file);
        mutex.unlock();
        return false;
    }
        secure_close(&sefile_file);
        mutex.unlock();
        return true;
}

bool SEfile_interface::getDecrypted(QDir path, QString filename, QString &filenameDecrypted){
   SEFILE_FHANDLE sefile_file;
   uint32_t plainFileLength, bytesRead;
   uint8_t *buffer;
   char filenameDecryptedChar[200];

   mutex.lock();
   QString encFilePath = path.filePath(filename);
   // Open encrypted file
   if(secure_open(encFilePath.toUtf8().data(), &sefile_file, SEFILE_READ, SEFILE_OPEN, false ) != 0){
     mutex.unlock();
     return false;
   }
   // Get the size of the resulting plain file
   if(secure_getopenfilesize(&sefile_file, &plainFileLength) != 0){
       secure_close(&sefile_file);
       mutex.unlock();
       return false;
   }
   // Get original filename
   if(secure_decrypt_filehandle(&sefile_file, filenameDecryptedChar) != 0){
       secure_close(&sefile_file);
       mutex.unlock();
       return false;
   }
   filenameDecrypted = QString(filenameDecryptedChar);

   buffer = (uint8_t *)calloc(plainFileLength, sizeof(uint8_t));
   if(secure_read(&sefile_file, buffer, plainFileLength, &bytesRead) != 0) {
        secure_close(&sefile_file);
        mutex.unlock();
        return false;
   }
   // Create a plain file to store the plain content
   QString plainFilePathComplete = path.filePath(filenameDecrypted);
   QFile *plainFile = new QFile(plainFilePathComplete);
   plainFile->open(QIODevice::WriteOnly);
   QByteArray *plainContent = new QByteArray((char*)buffer, bytesRead);
   plainFile->write(*plainContent);
   plainFile->close();
   secure_close(&sefile_file);
   mutex.unlock();
   return true;
}

bool SEfile_interface::changeKeyID(uint32_t keyID, uint16_t crypto = 0){
    return !secure_update(&s,keyID, crypto);
}

bool SEfile_interface::getKeyList(se3_key *list, uint16_t *count){
    return !L1_key_list(&s,0,5000,NULL,list,count);
}

uint32_t SEfile_interface::getKeyID(QString keyName){
    se3_key keys[5000];
    uint16_t keysNum;
    // Return the keyID if the username exists
    uint32_t id = keysList.value(keyName, 0);
    if(id > 0){
        return id;
    }
    keysList.clear();
    // If the username is not present download the list of keys from the board
    getKeyList(keys, &keysNum);
    for(int i=0; i < keysNum; i++){
        QString name = QString::fromUtf8((char*)keys[i].name, keys[i].name_size);
        keysList.insert(name, keys[i].id);
    }
    return keysList.value(keyName,0);
}

bool SEfile_interface::changeKeyID(QString keyName){
    uint32_t id = getKeyID(keyName);
    if(id > 0)
        return changeKeyID(id);
    else
        return false;
}

bool SEfile_interface::addKey(QString keyName, uint8_t *key, uint16_t keySize, uint32_t validity){
    uint32_t id;
    se3_key keyStruct;
    memcpy(keyStruct.name,keyName.toUtf8().data(), keyName.length());
    keyStruct.name_size = keyName.length();
    keyStruct.data_size = keySize;
    keyStruct.data = key;
    keyStruct.validity = (uint32_t)time(0) + validity;
    // Check if the key name is already present and modify the key
    if((id = getKeyID(keyName)) > 0){
        // The key already exists... Update it
        keyStruct.id = id;
    } else{
        // New key, append to the others
        // The local structure keysList has been updated before so I can directly check there
        // the first free slot
       keyStruct.id = (uint32_t)keysList.size() + 1;
    }
    return !L1_key_edit(&s, SE3_KEY_OP_UPSERT, &keyStruct);
}

bool SEfile_interface::deleteKey(QString keyName){
    se3_key keyStruct;
    uint32_t id = getKeyID(keyName);
    uint8_t empty[1] = {0};
    keyStruct.id = id;
    memcpy(keyStruct.name,keyName.toUtf8().data(), keyName.length());
    keyStruct.name_size = keyName.length();
    keyStruct.data = empty;
    keyStruct.data_size = 1;
    if(id == 0)
        return false;
    return !L1_key_edit(&s,SE3_KEY_OP_DELETE,&keyStruct);
}
