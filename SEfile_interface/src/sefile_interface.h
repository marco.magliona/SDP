#ifndef SEFILE_INTERFACE_H
#define SEFILE_INTERFACE_H

#include "SEfile.h"
#include <QFile>
#include <QMap>
#include <QDir>
#include <QMutex>

class SEfile_interface
{
public:

    /**
	 * @brief Constructor
	 *
	 * @param session Pointer to an open session
	 */
    SEfile_interface(se3_session* session);

    /**
     * @brief Logout from the board. Needed if we want to disconnect and then
     * login again without powercycling
     */
    void logout();

    /**
     * @brief Take a plain file as input and encrypt it using the current
     *  keyID. The encrypted file is placed in @param path .
     *
     * @param path [in] Path to the plain file
     * @param filename [in] Name of the plain file
     * @param filenameEncrypted [out] Name of the generated encrypted file
     *
     * @return True if the operation was successful, false otherwise
     */
    bool getEncrypted(QDir path, QString filename, QString &filenameEncrypted);

    /**
     * @brief Take an encrypted file and decrypt it. The decrypted file
     * is placed in @param path .
     *
     * @param path [in] Path to the encrypted file
     * @param filename [in] Name of the encrypted file
     * @param filenameDecrypted èout] Name of the generated decrypted file
     *
     * @return True if the operation was successful, false otherwise
     */
    bool getDecrypted(QDir path, QString filename, QString &filenameDecrypted);

    /**
     * @brief Download the keys from the board and save in the
     * preallocated @param list .
     *
     * @param list [out] Preallocated array of se3_key
     * @param count [out] Number of obtained keys
     *
     * @return
     */
    bool getKeyList(se3_key *list, uint16_t *count);

    /**
     * @brief Switch the active key to the one specified by
     * @param keyID. Crypto is the number of the crypto algo.
     *
     * @param keyID Key id to switch to
     * @param crypto Can be -1 or NULL
     *
     * @return
     */
    bool changeKeyID(uint32_t keyID, uint16_t crypto);

    /**
     * @brief Return the key ID associated with @param keyName
     *
     * @param keyName Name of the key to look for
     *
     * @return the key id number, 0 if not found
     */
    uint32_t getKeyID(QString keyName);

    /**
     * @brief Change to the key associated to @param keyName
     *
     * @param keyName
     *
     * @return True if the operation is successful, False otherwise
     */
    bool changeKeyID(QString keyName);

    /**
     * @brief Add or modify an already existent key
     *
     * @param keyName The name of the key (like username)
     * @param key The byte array of the key
     * @param keySize Key length
     * @param validity Number of seconds of validity starting from now
     *
     * @return
     */
    bool addKey(QString keyName, uint8_t *key, uint16_t keySize, uint32_t validity);

    /**
     * @brief Delete a key from the board
     *
     * @param keyName Name of the key to  delete
     *
     * @return True if the operation is successful, False otherwise
     */
    bool deleteKey(QString keyName);
private:
    se3_session s;
    QMap<QString, uint32_t> keysList;
    QMutex mutex;
};

#endif // SEFILE_INTERFACE_H
