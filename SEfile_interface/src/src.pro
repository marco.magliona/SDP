#-------------------------------------------------
#
# Project created by QtCreator 2016-11-08T16:58:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SEfile_interface
TEMPLATE = app


SOURCES += main.cpp\
        logindialog.cpp \
    sefile_interface.cpp \
    sefile_dialog.cpp

HEADERS  += \
    logindialog.h \
    sefile_interface.h \
    sefile_dialog.h


FORMS    += \
    dialog.ui

win32 {
    SEFILEPATH  = "$$OUT_PWD/../SEfile"
    CONFIG(debug,debug|release){
        SEFILEPATH = $$SEFILEPATH/debug
    }
    CONFIG(release,debug|release){
        SEFILEPATH = $$SEFILEPATH/release
    }
}
!win32 {
    SEFILEPATH  = "../SEfile"
}

LIBS += -L$$SEFILEPATH

LIBS += -lSEfile

INCLUDEPATH += "$$PWD/../SEfile"
DEPENDPATH += "$$PWD/../SEfile" \
            $$SEFILEPATH

CONFIG(release,debug|release){
    CONFIG += static
}
