#-------------------------------------------------
#
# Project created by QtCreator 2016-11-08T16:58:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SEfile_interface
TEMPLATE = lib

SOURCES += \
        logindialog.cpp \
    sefile_interface.cpp \
    sefile_dialog.cpp

HEADERS  += \
    logindialog.h \
    sefile_interface.h \
    sefile_dialog.h


FORMS    += \
    dialog.ui

linux-g++ {
    DEFINES += _GNU_SOURCE
}

HEADERS += \
    ../SEfile/se3/sha256.h \
    ../SEfile/se3/se3comm.h \
    ../SEfile/se3/se3c1def.h \
    ../SEfile/se3/se3c0def.h \
    ../SEfile/se3/se3_common.h \
    ../SEfile/se3/pbkdf2.h \
    ../SEfile/se3/L1.h \
    ../SEfile/se3/L0.h \
    ../SEfile/se3/crc16.h \
    ../SEfile/se3/aes256.h \
    ../SEfile/SEfile.h


SOURCES += \
    ../SEfile/se3/sha256.c \
    ../SEfile/se3/aes256.c \
    ../SEfile/se3/crc16.c \
    ../SEfile/se3/L0.c \
    ../SEfile/se3/L1.c \
    ../SEfile/se3/se3_common.c \
    ../SEfile/se3/se3comm.c \
    ../SEfile/se3/pbkdf2.c \
    ../SEfile/SEfile.c

INCLUDEPATH += "$$PWD/../SEfile"
DEPENDPATH += "$$PWD/../SEfile" \
            $$SEFILEPATH

CONFIG += staticlib
