# 2 - Securing real-time Messaging Services

## Gantt
![Gantt](securing-real-time-messaging-services-gantt.png)

## Deliverables
* This presentation
* Requirement document
* SEfile modified C library
* SEfile Cpp/QT wrapper
* SEfile Telegram single executable
* Documentation

## Issues
\tiny
\centering
\begin{tabular}{|c|c|c|c|p{2.5cm}|c|c|c|}
\hline
& Tracker & Status & Pri & Subject & Prog. & Start date & Due date \\
\hline
40 & Support & Closed      & Imm & 3 slides presentation                                        & $100 \%$ & 04/22& 04/26 \\
\hline
37 & Support & In Progress & Nor & Finalize gantt and writedown scenarios                       & $90 \%$ & 04/11& 04/24 \\
\hline
33 & Support & New         & Nor & Documentation                                                & $0 \%$ & 05/10& 06/16 \\
\hline
32 & Feature & New         & Nor & Implementation user-key association                          & $0 \%$ & 05/19& 06/02 \\
\hline
31 & Feature & New         & Nor & Extension of communication protocol to groups                & $0 \%$ & 05/19& 05/26 \\
\hline
30 & Feature & New         & Nor & Implementation of user to user communication                 & $0 \%$ & 05/10& 05/18 \\
\hline
29 & Feature & New         & Nor & Integration of Telegram and SEfile                           & $0 \%$ & 05/10& 06/02 \\
\hline
27 & Feature & In Progress & Nor & Telegram code analysis                                       & $70 \%$ & 04/12& 05/09 \\
\hline
26 & Feature & In Progress & Nor & Implement missing functions in SEfile SDK (L2)               & $90 \%$ & 04/12& 05/01 \\
\hline
25 & Feature & Closed      & Nor & Development environment setting (SDKs) and API familiarizing & $100 \%$ & 04/05& 04/12 \\
\hline
20 & Feature & Closed      & Urg & Initial briefing and SDK check                               & $100 \%$ & 03/23&  \\
\hline
\end{tabular}
