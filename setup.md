# Setting Up SElegram

**This procedure has only been tested on GNU/Linux.**

## Download the source

Fetch this repository using

```bash
    git clone --recursive https://gitlab.com/marco.magliona/SDP.git
```

## Compiling guide

### Native compilation (the produced executable can be used directly)
Navigate to the *telegram* folder with

```bash
    cd telegram
```

Please create the **Libraries** folder inside the **telegram** folder.
Follow the normal Telegram compilation instruction [here](https://gitlab.com/marco.magliona/tdesktop/blob/selegram/docs/building-cmake.md).

The building system of Telegram has been modified to include, compile and link the SEfile libraries.
The process is fully automated and exactly the same as the one for standard Telegram.

### Docker compilation (the produced executable can only be used inside Docker)
We have a preconfigured Docker image with the development environment configured and all the libraries installed. You can download it [here](https://mega.nz/#!89sjXBDT!9Cb_BUemhdSl7aAnXMhFs-6rTvIIeV7SEA2diPAK_d8) (DO NOT UNPACK THE tar FILE).
You can download the precompiled libraries to be used with the docker image [here](https://mega.nz/#!x4lVXLJI!Izk-2fxmiSrcM0ILa-6cOFGyyL2HhRE_1ChoVT2uVls) and unpack them inside *telegram* folder.

Import the docker image with  
```bash
    docker load -i telegramdev.tar
```

You can run it with

```bash
    docker run -t -i --rm --privileged -v /<Path to the downloaded SDP repository>/SDP:/SDP \  
    -v /<Path to where the removable devices are mounted>:/run/media -e DISPLAY=$DISPLAY \  
    -v /tmp/.X11-unix:/tmp/.X11-unix telegram_dev:latest /bin/bash
```

Inside the root of the container is present a script which launches the compilation and produces the final SElegram executable.

```bash
   ./compileTelegram.sh
```

## Telegram autogenenerated documentation
The documentation, generated in Doxygen, we have used to analize the Telegram source code can be downloaded from [here](https://mega.nz/#!Rg0jlR4B!_d_Re616HBikhKXy52JZ_v9k4BHHztHH7KaLamtUF4A). Uncompress the archive and open *index.html* .
