# User Manual

**NOTE 1: This procedure has only been tested on GNU/Linux.**

**NOTE 2: You have to complete the procedure explained in** *setup.pdf* **before running SElegram.**

## Running SElegram


__Case 1__: You have compiled using the Docker image provided.

* Connect the board before launching the Docker image

* Run the command 
```bash
   xhost +local:docker
```

*  Run the Docker image as shown above and when you get the shell prompt launch SElegram with 
```bash
    	Telegram
```
(If nothing is shown, close the Docker container and run the command 
```bash
		xhost +
```
then repeat this step).

__Case 2__: You are not using the provided Docker image

* Connect the board

* Run ```cd <Path to the SDP folder>/telegram/out/Debug```

* then ```./Telegram```

## Login 

Once SElegram has been launched a login prompt to enable SEfile is shown. Insert the password associated with the SEcube board. 

## Change download folder

In order to make working the encryption/decryption is important to set the Download folder in Telegram to **/tmp/**. Set this option in Settings menu -> Download Path -> Custom Folder.

## Sending messages
No differences with the normal Telegram application. Please notice that two *"message box"* per each sent message will be generated.

## Reading incoming messages
In order to decrypt and read the content of an incoming messages you need to manually download the received file (by design).

## Logout
If you close the logout window, you can not encrypt or decrypt message anymore.

